# Level AmstradCPC

## Level 1

- [SPRITE](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/first_sprite/INSTRUCTION.md?ref_type=heads)

## Level 2

- [CHALLENGE 1.2 - 1.3 ANIMATE YOUR PIXELS AND CREATE YOUR OWN ANIMATION](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/animation/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 2.1 IMITATE A SHOT](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/shot/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 2.2 IMITATE A TRAP](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/game_trap/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 2.3 THE ACCELERATED BALL](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/accelerated_ball/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 3.1 DRAW YOUR FLOOR](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/challenge31/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 3.2 FLOOR THROUGHOUT THE SCREEN](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/challenge32/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 3.3 DRAW THE SKY](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/challenge33/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 3.4 CREATE YOUR SHOOTING CHARACTER (GREAT CHALLENGE)](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/challenge34/INSTRUCTION.md?ref_type=heads)
- [POSITIONS AND VARIABLES - ARKANOID BAR](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/arkanoid_bar/INSTRUCTION.md?ref_type=heads)
- [CHALLENGE 4.1 CREATE A FLOOR WITH PERIODIC REPETITION](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/challenge41/INSTRUCTION.md?ref_type=heads)



## Level 3 
