# README ARKANOID_BAR   

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/arkanoid_bar/b_arkanoid.gif?ref_type=heads)

## Code Explanation Diagram

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_challenges_explained/arkanoid_bar/b_complete_arkanoid.svg?ref_type=heads)



## INSTRUCTIONS

- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4205, set all values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/arkanoid_bar/arkanoid_bar.png?ref_type=heads)

- The video of the result of its operation and it can also be seen
below this document in a gif format.

- Attaching the bin file as b_arkanoid_complete.bin

## The challenge:

- Draw and animate an arkanoid bar that uses the entire screen both going and coming (in both directions)


## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/arkanoid_bar/b_arkanoid1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/arkanoid_bar/b_arkanoid2.png?ref_type=heads)
