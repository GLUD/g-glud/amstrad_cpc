# GAME TRAP

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/game_trap/gifs/gametrap.gif?ref_type=heads)


## INSTRUCTIONS
- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4000, set all values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/game_trap/trap.png?ref_type=heads)

- The video of the result of its operation and it can also be seen
below this document in a gif format.

## The challenge:

- 2 rows of pixels tall (Bigger if you want).
- Static part: 4 pixels wide.
- Moving part: 16 pixels wide.
- 3 frames up and 3 down.
- Each frame lasts 3 tenths of a second.

Bonuses:

- Powerup: Bigger Trap.
Optimizer: Your code is 32 bytes per frame (or less)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/game_trap/gifs/challenge.gif?ref_type=heads)


## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/game_trap/trap1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/game_trap/trap2.png?ref_type=heads)