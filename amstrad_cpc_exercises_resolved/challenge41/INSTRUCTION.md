#  CHALLENGE 4.1   

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge41/gifs/challenge41.gif?ref_type=heads)

## Explanation by flowchart:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_challenges_explained/challenge41/challenge41.svg?ref_type=heads)

## INSTRUCTIONS

- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4000, set all values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge41/challenge41.png?ref_type=heads)

- The video of the result of its operation and it can also be seen below this document in a gif format.

- Attaching the bin file as challenge41_animated_complete.bin

## The challenge 4.1:

- We will draw 9 times the type 0 tile and 1 the type 1 tile.

- Bonuses: Multiple; Minimum 3 types of tiles and 2 of them must be repeated at least 4 times each.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge41/gifs/challenge41exercise.png?ref_type=heads)

## The images of the codes to insert them are:


![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge41/1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge41/2.png?ref_type=heads)
