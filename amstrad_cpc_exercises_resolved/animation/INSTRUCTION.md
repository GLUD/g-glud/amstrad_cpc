# ANIMATION

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/gifs/animation.gif?ref_type=heads)

## INSTRUCTIONS

- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4000, set all values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/animation.png?ref_type=heads)

- The video of the result of its operation and it can also be seen
below this document in a gif format.

## Double challenge:

* Challenge 1:

- Fill from left to right
- It must be repeated infinitely.
- Use all 4 colors.

Bonuses:

- Pixel addict: make your bar larger than 4 pixels.
  
![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/gifs/challenge.gif?ref_type=heads)

* Challenge 2:

- Use only the instructions allowed for this challenge.

- Minimum 4 x 4 pixels (bigger if you want).

Bonuses:

- Framemaster: Make an animation with more than 4 frames.
- Big Changes: In each frame you only write the bytes that change.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/gifs/challenge2.gif?ref_type=heads)


## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/a1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/a2.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/animation/a3.png?ref_type=heads)