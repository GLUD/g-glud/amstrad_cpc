# FIRST SPRITE

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/first_sprite/mario.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/first_sprite/first_sprite.png?ref_type=heads)

## INSTRUCTIONS


- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4000, set all values to zero and start Winape.

- The result of its operation and it can also be seen below this document in a png format.

## The Challenge

- Final challenge of the first level in which you have to paint a sprite to be able to overcome it. The sprite must be 16x16 pixels and be in the center of the screen.


## The images of the code to instert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/first_sprite/sprite_codigo.png?ref_type=heads)