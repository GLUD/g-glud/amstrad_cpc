#  CHALLENGE 3.4   

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/gifs/RESULTS.gif?ref_type=heads)


## INSTRUCTIONS

- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4300, set all values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/mario_challenge34.png?ref_type=heads)

- The video of the result of its operation and it can also be seen below this document in a gif format.

- Attaching the bin file as MARIO_CHALLENGES34.bin

## The challenge 3.4:

- Add an 8 x 8 pixel character to all the code above.

- Add a bullet that travels across the screen.

- Add a 4 x 4 pixel burst at the end of the bullet path.

- Bonuses: Animator: If you make your character animated or if instead of an explosion you create an enemy, also animated, that receives the bullet and, for example, falls when dying.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/gifs/challenge.gif?ref_type=heads)




## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/cod1.png?ref_type=heads)


![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/cod2.png?ref_type=heads)


![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/cod3.png?ref_type=heads)


![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/cod4.png?ref_type=heads)


![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge34/cod5.png?ref_type=heads)


