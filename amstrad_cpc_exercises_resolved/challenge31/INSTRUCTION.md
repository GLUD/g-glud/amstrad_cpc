# CHALLENGE 3.1

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/challenge31/Tileable.png?ref_type=heads)

## INSTRUCTIONS

- All code is placed (or pasted) at position 4000

- The result of its operation can also be seen below this document in a png format.

## The challenge 3.1

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge31/challenge31exercise.png?ref_type=heads)

- Create a 4 x 4 pixel floor design.

- You must use instruction 36. Your floor should be "Tileable"

## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge31/c_tileable.png?ref_type=heads)

