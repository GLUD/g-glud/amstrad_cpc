# ACCELERATED BALL

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/accelerated_ball/gifs/accelerated_ball.gif?ref_type=heads)


## INSTRUCTIONS

- All code is placed (or pasted) at position 4000.

- The PC (Program Counter) starts at 4000, set all values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/accelerated_ball/accelerated_ball.png?ref_type=heads)

- The video of the result of its operation and it can also be seen below this document in a gif format.


## The challenge:

- Minimum 4x4 pixels.
- Movement: 4 pixels within the first character.
- Above the letter "A".
- Use instructions 36 and 26 to draw.
- Use the 21 maximum statement once.

## Bonuses:

- Accelerated: Simulates gravity, accelerating and decelerating.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/accelerated_ball/gifs/challenge.gif?ref_type=heads)


## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/accelerated_ball/pelota_acelerada.png?ref_type=heads)