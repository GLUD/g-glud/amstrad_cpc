# CHALLENGE 3.3

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/challenge33/challenge33zoom.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/challenge33/challenge33.png?ref_type=heads)

## INSTRUCTIONS

- All code is placed (or pasted) at position 4000

- The PC (Program Counter) starts at 407B, set values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/challenge33_flags.png?ref_type=heads)

- the result of its operation can also be seen below this document in a png format.

## The challenge 3.3

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/gifs/challenge33exercise.png?ref_type=heads)

- Draw a 16 x 4 pixel sky.

- Continue to use the code from the previous activity.

Bonuses:

- Pixelator: If your sky is more than 4 pixels high.

## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/cod33.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/code1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/code2.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/code3.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/code4.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge33/code5.png?ref_type=heads)
