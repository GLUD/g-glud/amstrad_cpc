# CHALLENGE 3.2

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/challenge32/challenge32zoom.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/amstrad_cpc_exercises_resolved/challenge32/challenge32.png?ref_type=heads)

## INSTRUCTIONS

- All code is placed (or pasted) at position 4000

- The PC (Program Counter) starts at 401F, set values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge32/challenge32.png?ref_type=heads)

- the result of its operation can also be seen below this document in a png format.

## The challenge 3.2

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge32/gifs/challenge32exercise.gif?ref_type=heads)

- The code to draw must be unique and repeated using a loop.

Bonuses:

- Minimalist30: If your code occupies 30 bytes.
- Depths: If your floor is more than 4 pixels high.

## The images of the codes to insert them are:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge32/1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/challenge32/2.png?ref_type=heads)