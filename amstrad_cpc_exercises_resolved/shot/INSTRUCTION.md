# CHALLENGE 2.1 IMITATES A SHOT

## The results is:

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/shot/gifs/shot.gif?ref_type=heads)


## INSTRUCTIONS

- All code is placed (or pasted) at position 4000

- The PC (Program Counter) starts at 4000, set values to zero and start Winape.

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/shot/shot_flags.png?ref_type=heads)



- the result of its operation can also be seen below this document in a gif format.

## The challenge 2.1

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/shot/gifs/challenge21exercise.gif?ref_type=heads)

- 4-pixel bullet.
- It moves from 4 to 4 pixels.
- Each frame lasts 3 tenths of a second.

Bonuses:

- Super16Bits: Use instructions 21 and 22 to draw.

## The images of the codes to insert them are:


![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/shot/shot1.png?ref_type=heads)

![](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/shot/shot2.png?ref_type=heads)