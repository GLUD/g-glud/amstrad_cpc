# Amstrad CPC MACHINE CODE

### Angel Granados


<div style="display: inline_block"><br>
    <a href="https://profesorretroman.com/" target="_blank"><img align="center" alt="AmstradCPC"   src="https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/logo.png?ref_type=heads">
</div>

Particular exercises and completed challenges of the Retroman Program.

<div style="text-align: justify"> I present the approach together with the solutions of the different challenges. All the codes are in .bin format and must be loaded in position 4000 of the WINAPE program (CPCTelera). The start of the PC (Program Counter) will be indicated in the solution of each challenge in INSTRUCTION.md enjoy the program with its approaches and its solutions. </div>


- :fire: [EXPLANATIONS OR RESULTS (OR BOTH)](https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/tree/main/images/level_amstrad?ref_type=heads) :point_left: :fire:  


## Medals

Click on the image and you will see the results of the medal

<div style="display: inline_block"><br>
    <a href="https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/blob/main/amstrad_cpc_exercises_resolved/first_sprite/INSTRUCTION.md?ref_type=heads" target="_blank"><img align="center" alt="AmstradCPC" height="120" width="120" src="https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/medals/medal_machine_code_first_sprite.png?ref_type=heads">
</div>

## Resources

<div style="text-align: right"> It is a program that can be found on the channel: </div>

- [YOUTUBE RETROMAN](https://www.youtube.com/@ProfesorRetroman/about)

- [PROFESOR RETROMAN](https://profesorretroman.com/)

<div style="text-align: right"> With a complete machine language course and assembler on Z80. All executable files are in binary format and are executed in winape. If you want to install and test the binary, here is the video supported by the CPCtelera community:</div>

- [OS-LINUX UBUNTU](https://www.youtube.com/watch?v=aItoD7OfdkM)

- [MAC OSX](https://www.youtube.com/watch?v=4a_dh0WXWic)

- [OS-WINDOWS](https://www.youtube.com/watch?v=YIrbPEinYp0&list=RDCMUCSdIAKvPxlB3VlFDCBvI46A&index=3)

## Winape Amstrad CPCtelera

<div style="display: inline_block"><br>
    <a href="http://www.winape.net/downloads.jsp" target="_blank"><img align="center" alt="AmstradCPC" src="https://gitlab.com/GLUD/g-glud/amstrad_cpc/-/raw/main/images/amstradcpc.png?ref_type=heads">
</div>


